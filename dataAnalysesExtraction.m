clear;

%% Parameters setting

SPEAKER = 'Adam';
TEST_FILE = '09';
PERSON_TYPE = 'e';
HIDDEN_UNITS = '64';
HIDDEN_LAYERS = 1;

%% Parameters likely stable

FILRTER_FRAME = 201;
HEAD_DIMENSION = 9;
FILE_DIR = '/home/spiderpete/Documents/Dissertation/System_2.0/results';
SPEAKER_DIR = [FILE_DIR, '/', SPEAKER];
INTERLAYED_DIR = [SPEAKER_DIR, '/', 'Interlayed'];
NON_SMOOTHED_DIR = [SPEAKER_DIR, '/', 'Non-Smoothed'];
SMOOTHED_DIR = [SPEAKER_DIR, '/', 'Smoothed'];

%% Extract stats

hiddenUnitsString = HIDDEN_UNITS;
if HIDDEN_LAYERS ~= 1
    for i = 2 : HIDDEN_LAYERS
        hiddenUnitsString = [hiddenUnitsString, ',', HIDDEN_UNITS];
    end
end

ground = dlmread([SPEAKER, '_', TEST_FILE, '_', PERSON_TYPE, '.ea132+delta2.dat']);
result = dlmread([SPEAKER, '_', TEST_FILE, '_', PERSON_TYPE, '-[', hiddenUnitsString, '].tmp']);

smooth = sgolayfilt(result, 2, FILRTER_FRAME);

diff0 = ground - result;
diff = ground - smooth;

[A0, B0, r0, U0, V0] = canoncorr(ground, result);
[A, B, r, U, V] = canoncorr(ground, smooth);

disp('====================================================================================================================================');
disp(['Cannonical correlation NON-smoothed: ', num2str(r0(1))]);
disp(['Cannonical correlation smoothed: ', num2str(r(1))]);
disp('====================================================================================================================================');

coef = zeros(1, HEAD_DIMENSION);
for dim = 1 : HEAD_DIMENSION
    temp = corrcoef(ground(:,dim), result(:,dim));
    coef(dim) = temp(1,2);
end
disp(['Pearson correlation NON-smoothed: ', ...
    num2str(coef(1)), ' ', num2str(coef(2)), ' ', num2str(coef(3)), ' ' ...
    num2str(coef(4)), ' ', num2str(coef(5)), ' ', num2str(coef(6)), ' ', ...
    num2str(coef(7)), ' ', num2str(coef(8)), ' ', num2str(coef(9))]);

coef2 = zeros(1, HEAD_DIMENSION);
for dim = 1 : HEAD_DIMENSION
    temp2 = corrcoef(ground(:,dim), smooth(:,dim));
    coef2(dim) = temp2(1,2);
end
disp(['Pearson correlation smoothed: ', ...
    num2str(coef2(1)), ' ', num2str(coef2(2)), ' ', num2str(coef2(3)), ' ', ...
    num2str(coef2(4)), ' ', num2str(coef2(5)), ' ', num2str(coef2(6)), ' ', ...
    num2str(coef2(7)), ' ', num2str(coef2(8)), ' ', num2str(coef2(9))]);
disp('====================================================================================================================================');

%% Save plots

clf;
plot(U0(:,1), V0(:,1),'.');
saveas(gcf, fullfile(NON_SMOOTHED_DIR, ['Canonical variables - ', hiddenUnitsString]), 'jpg');

clf;
plot(U(:,1), V(:,1),'.');
saveas(gcf, fullfile(SMOOTHED_DIR, ['Canonical variables - ', hiddenUnitsString]), 'jpg');

clf;
plot(U0(:,1), V0(:,1),'r.', U(:,1), V(:,1), 'g.');
saveas(gcf, fullfile(INTERLAYED_DIR, ['Canonical variables - ', hiddenUnitsString]), 'jpg');

xAxis = linspace(1, size(result, 1), size(result, 1));
for i = 1 : HEAD_DIMENSION
    clf;
    plot(xAxis, diff0(:, i));
    saveas(gcf, fullfile(NON_SMOOTHED_DIR, ['Diff', num2str(i), ' - ', hiddenUnitsString]), 'jpg');
end

for i = 1 : HEAD_DIMENSION
    clf;
    plot(xAxis, diff(:, i));
    saveas(gcf, fullfile(SMOOTHED_DIR, ['Diff', num2str(i), ' - ', hiddenUnitsString]), 'jpg');
end

for i = 1 : HEAD_DIMENSION
    clf;
    plot(xAxis, diff0(:, i), 'r', xAxis, diff(:,i), 'g');
    saveas(gcf, fullfile(INTERLAYED_DIR, ['Diff', num2str(i), ' - ', hiddenUnitsString]), 'jpg');
end

disp('Successfully Completed!');
disp('====================================================================================================================================');

clear;