#!/bin/bash

# Make toy data set to try out Zhizheng's DNN code with head motion prediction
#
#
#

## Set this to point to SPTK's x2x:
X2X=/afs/inf.ed.ac.uk/group/cstr/projects/galatea/tools/SPTK-3.7/bin/x2x
TOPDATADIR=/afs/inf.ed.ac.uk/group/cstr/projects/galatea/PeterS/dnn/dnn_tts_lstm/data
SPEAKER=Esmo
TYPE=e
IDS=(02 04 07 09)

HEADDIR=$TOPDATADIR/original/${SPEAKER}/head

MFCCDIR=$TOPDATADIR/original/${SPEAKER}/mfcc.pitch

mkdir -p $TOPDATADIR/$SPEAKER
mkdir -p $TOPDATADIR/${SPEAKER}/mfcc
mkdir -p $TOPDATADIR/${SPEAKER}/head

## Get number of lines per file
NUMOFLINES1_0=$(wc -l < "$MFCCDIR/${SPEAKER}_${IDS[0]}_${TYPE}.dat")
NUMOFLINES1_1=$(wc -l < "$HEADDIR/${SPEAKER}_${IDS[0]}_${TYPE}.ea132+delta2")
NUMOFLINES1=$((NUMOFLINES1_0<NUMOFLINES1_1?NUMOFLINES1_0:NUMOFLINES1_1))

NUMOFLINES2_0=$(wc -l < "$MFCCDIR/${SPEAKER}_${IDS[1]}_${TYPE}.dat")
NUMOFLINES2_1=$(wc -l < "$HEADDIR/${SPEAKER}_${IDS[1]}_${TYPE}.ea132+delta2")
NUMOFLINES2=$((NUMOFLINES2_0<NUMOFLINES2_1?NUMOFLINES2_0:NUMOFLINES2_1))

NUMOFLINES3_0=$(wc -l < "$MFCCDIR/${SPEAKER}_${IDS[2]}_${TYPE}.dat")
NUMOFLINES3_1=$(wc -l < "$HEADDIR/${SPEAKER}_${IDS[2]}_${TYPE}.ea132+delta2")
NUMOFLINES3=$((NUMOFLINES3_0<NUMOFLINES3_1?NUMOFLINES3_0:NUMOFLINES3_1))

NUMOFLINES4_0=$(wc -l < "$MFCCDIR/${SPEAKER}_${IDS[3]}_${TYPE}.dat")
NUMOFLINES4_1=$(wc -l < "$HEADDIR/${SPEAKER}_${IDS[3]}_${TYPE}.ea132+delta2")
NUMOFLINES4=$((NUMOFLINES4_0<NUMOFLINES4_1?NUMOFLINES4_0:NUMOFLINES4_1))

## Use 28k frames for training (utterance 01), 1k for training and testing (2 and 3)
sed -n 1,${NUMOFLINES1}p "$MFCCDIR/${SPEAKER}_${IDS[0]}_${TYPE}.dat" |  tr '\t' '\n'  | $X2X +af >  "$TOPDATADIR/${SPEAKER}/mfcc/${SPEAKER}_${IDS[0]}_${TYPE}.mfcc"
sed -n 1,${NUMOFLINES2}p "$MFCCDIR/${SPEAKER}_${IDS[1]}_${TYPE}.dat" |  tr '\t' '\n'  | $X2X +af >  "$TOPDATADIR/${SPEAKER}/mfcc/${SPEAKER}_${IDS[1]}_${TYPE}.mfcc"
sed -n 1,${NUMOFLINES3}p "$MFCCDIR/${SPEAKER}_${IDS[2]}_${TYPE}.dat" |  tr '\t' '\n'  | $X2X +af >  "$TOPDATADIR/${SPEAKER}/mfcc/${SPEAKER}_${IDS[2]}_${TYPE}.mfcc"
sed -n 1,${NUMOFLINES4}p "$MFCCDIR/${SPEAKER}_${IDS[3]}_${TYPE}.dat" |  tr '\t' '\n'  | $X2X +af >  "$TOPDATADIR/${SPEAKER}/mfcc/${SPEAKER}_${IDS[3]}_${TYPE}.mfcc"


## for now, pretend that mfccs are angles:

sed -n 1,${NUMOFLINES1}p "$HEADDIR/${SPEAKER}_${IDS[0]}_${TYPE}.ea132+delta2" |  tr '\t' '\n'  | $X2X +af >   "$TOPDATADIR/${SPEAKER}/head/${SPEAKER}_${IDS[0]}_${TYPE}.head"
sed -n 1,${NUMOFLINES2}p "$HEADDIR/${SPEAKER}_${IDS[1]}_${TYPE}.ea132+delta2" |  tr '\t' '\n'  | $X2X +af >   "$TOPDATADIR/${SPEAKER}/head/${SPEAKER}_${IDS[1]}_${TYPE}.head"
sed -n 1,${NUMOFLINES3}p "$HEADDIR/${SPEAKER}_${IDS[2]}_${TYPE}.ea132+delta2" |  tr '\t' '\n'  | $X2X +af >   "$TOPDATADIR/${SPEAKER}/head/${SPEAKER}_${IDS[2]}_${TYPE}.head"
sed -n 1,${NUMOFLINES4}p "$HEADDIR/${SPEAKER}_${IDS[3]}_${TYPE}.ea132+delta2" |  tr '\t' '\n'  | $X2X +af >   "$TOPDATADIR/${SPEAKER}/head/${SPEAKER}_${IDS[3]}_${TYPE}.head"


## make list of training data utterances:
echo ${SPEAKER}_02_e > $TOPDATADIR/${SPEAKER}/filelist_${SPEAKER}.txt
echo ${SPEAKER}_04_e >> $TOPDATADIR/${SPEAKER}/filelist_${SPEAKER}.txt
echo ${SPEAKER}_07_e >> $TOPDATADIR/${SPEAKER}/filelist_${SPEAKER}.txt
echo ${SPEAKER}_09_e >> $TOPDATADIR/${SPEAKER}/filelist_${SPEAKER}.txt
