
### refer Zhizheng and Simon's ICASSP 16 submission for more details

import numpy as np
import theano
import theano.tensor as T
from theano import config

class VanillaRNN(object):
    def __init__(self, rng, x, n_in, n_h):
    
        self.input = x        
                
        self.n_in = int(n_in)
        self.n_h = int(n_h)
        
        # random initialisation 
        Wx_value = np.asarray(rng.normal(0.0, 1.0/np.sqrt(n_in), size=(n_in, n_h)), dtype=config.floatX)
        Wh_value = np.asarray(rng.normal(0.0, 1.0/np.sqrt(n_h), size=(n_h, n_h)), dtype=config.floatX)

        # Input gate weights
        self.W_xi = theano.shared(value=Wx_value, name='W_xi')
        self.W_hi = theano.shared(value=Wh_value, name='W_hi')

        # bias
        self.b_i = theano.shared(value=np.zeros((n_h, ), dtype=config.floatX), name='b_i')
        
        
        # initial value of hidden and cell state
        self.h0 = theano.shared(value=np.zeros((n_h, ), dtype = config.floatX), name = 'h0')
        self.c0 = theano.shared(value=np.zeros((n_h, ), dtype = config.floatX), name = 'c0')


        self.Wix = T.dot(self.input, self.W_xi)
        
        [self.h, self.c], _ = theano.scan(self.lstm_as_activation_function, sequences = [self.Wix],
                                                             outputs_info = [self.h0, self.c0]) 

        self.output = self.h
        
        self.params = [self.W_xi, self.W_hi, self.b_i]
        
        self.L2_cost = (self.W_xi ** 2).sum() + (self.W_hi ** 2).sum()


    def lstm_as_activation_function(self, Wix, h_tm1, c_tm1):
        h_t = T.tanh(Wix + T.dot(self.W_hi, h_tm1) + self.b_i)  #

        c_t = h_t

        return h_t, c_t



class LstmBase(object):

    def __init__(self, rng, x, n_in, n_h):
    
        self.input = x        
                
        self.n_in = int(n_in)
        self.n_h = int(n_h)
        
        # random initialisation 
        Wx_value = np.asarray(rng.normal(0.0, 1.0/np.sqrt(n_in), size=(n_in, n_h)), dtype=config.floatX)
        Wh_value = np.asarray(rng.normal(0.0, 1.0/np.sqrt(n_h), size=(n_h, n_h)), dtype=config.floatX)
        Wc_value = np.asarray(rng.normal(0.0, 1.0/np.sqrt(n_h), size=(n_h, )), dtype=config.floatX)

        # Input gate weights
        self.W_xi = theano.shared(value=Wx_value, name='W_xi')
        self.W_hi = theano.shared(value=Wh_value, name='W_hi')
        self.w_ci = theano.shared(value=Wc_value, name='w_ci')

        # random initialisation 
        Wx_value = np.asarray(rng.normal(0.0, 1.0/np.sqrt(n_in), size=(n_in, n_h)), dtype=config.floatX)
        Wh_value = np.asarray(rng.normal(0.0, 1.0/np.sqrt(n_h), size=(n_h, n_h)), dtype=config.floatX)
        Wc_value = np.asarray(rng.normal(0.0, 1.0/np.sqrt(n_h), size=(n_h, )), dtype=config.floatX)

        # Forget gate weights
        self.W_xf = theano.shared(value=Wx_value, name='W_xf')
        self.W_hf = theano.shared(value=Wh_value, name='W_hf')
        self.w_cf = theano.shared(value=Wc_value, name='w_cf')

        # random initialisation 
        Wx_value = np.asarray(rng.normal(0.0, 1.0/np.sqrt(n_in), size=(n_in, n_h)), dtype=config.floatX)
        Wh_value = np.asarray(rng.normal(0.0, 1.0/np.sqrt(n_h), size=(n_h, n_h)), dtype=config.floatX)
        Wc_value = np.asarray(rng.normal(0.0, 1.0/np.sqrt(n_h), size=(n_h, )), dtype=config.floatX)

        # Output gate weights
        self.W_xo = theano.shared(value=Wx_value, name='W_xo')
        self.W_ho = theano.shared(value=Wh_value, name='W_ho')
        self.w_co = theano.shared(value=Wc_value, name='w_co')

        # random initialisation 
        Wx_value = np.asarray(rng.normal(0.0, 1.0/np.sqrt(n_in), size=(n_in, n_h)), dtype=config.floatX)
        Wh_value = np.asarray(rng.normal(0.0, 1.0/np.sqrt(n_h), size=(n_h, n_h)), dtype=config.floatX)
        Wc_value = np.asarray(rng.normal(0.0, 1.0/np.sqrt(n_h), size=(n_h, )), dtype=config.floatX)

        # Cell weights
        self.W_xc = theano.shared(value=Wx_value, name='W_xc')
        self.W_hc = theano.shared(value=Wh_value, name='W_hc')

        # bias
        self.b_i = theano.shared(value=np.zeros((n_h, ), dtype=config.floatX), name='b_i')
        self.b_f = theano.shared(value=np.zeros((n_h, ), dtype=config.floatX), name='b_f')
        self.b_o = theano.shared(value=np.zeros((n_h, ), dtype=config.floatX), name='b_o')
        self.b_c = theano.shared(value=np.zeros((n_h, ), dtype=config.floatX), name='b_c')
        
        ### make a layer
        
        # initial value of hidden and cell state
        self.h0 = theano.shared(value=np.zeros((n_h, ), dtype = config.floatX), name = 'h0')
        self.c0 = theano.shared(value=np.zeros((n_h, ), dtype = config.floatX), name = 'c0')


        self.Wix = T.dot(self.input, self.W_xi)
        self.Wfx = T.dot(self.input, self.W_xf)
        self.Wcx = T.dot(self.input, self.W_xc)
        self.Wox = T.dot(self.input, self.W_xo)
        
        [self.h, self.c], _ = theano.scan(self.recurrent_fn, sequences = [self.Wix, self.Wfx, self.Wcx, self.Wox],
                                                             outputs_info = [self.h0, self.c0]) 

        self.output = self.h
        
        
    def recurrent_fn(self, Wix, Wfx, Wcx, Wox, h_tm1, c_tm1 = None):
        h_t, c_t = self.lstm_as_activation_function(Wix, Wfx, Wcx, Wox, h_tm1, c_tm1)
            
        return h_t, c_t

    def lstm_as_activation_function(self):
        pass

class VanillaLstm(LstmBase):

    def __init__(self, rng, x, n_in, n_h):
    
        LstmBase.__init__(self, rng, x, n_in, n_h)
        
        self.params = [self.W_xi, self.W_hi, self.w_ci,
                       self.W_xf, self.W_hf, self.w_cf,
                       self.W_xo, self.W_ho, self.w_co, 
                       self.W_xc, self.W_hc,
                       self.b_i, self.b_f, self.b_o, self.b_c]
                       
    def lstm_as_activation_function(self, Wix, Wfx, Wcx, Wox, h_tm1, c_tm1):
    
        i_t = T.nnet.sigmoid(Wix + T.dot(h_tm1, self.W_hi) + self.w_ci * c_tm1 + self.b_i)  #
        f_t = T.nnet.sigmoid(Wfx + T.dot(h_tm1, self.W_hf) + self.w_cf * c_tm1 + self.b_f)  # 
    
        c_t = f_t * c_tm1 + i_t * T.tanh(Wcx + T.dot(h_tm1, self.W_hc) + self.b_c)

        o_t = T.nnet.sigmoid(Wox + T.dot(h_tm1, self.W_ho) + self.w_co * c_t + self.b_o)
                            
        h_t = o_t * T.tanh(c_t)

        return h_t, c_t#, i_t, f_t, o_t

class LstmNFG(LstmBase):

    def __init__(self, rng, x, n_in, n_h):

        LstmBase.__init__(self, rng, x, n_in, n_h)

        self.params = [self.W_xi, self.W_hi, self.w_ci,
                       self.W_xo, self.W_ho, self.w_co, 
                       self.W_xc, self.W_hc,
                       self.b_i, self.b_o, self.b_c]
                       
    def lstm_as_activation_function(self, Wix, Wfx, Wcx, Wox, h_tm1, c_tm1):
    
        i_t = T.nnet.sigmoid(Wix + T.dot(self.W_hi, h_tm1) + self.w_ci * c_tm1 + self.b_i)  #
    
        c_t = c_tm1 + i_t * T.tanh(Wcx + T.dot(self.W_hc, h_tm1) + self.b_c)  #f_t * 

        o_t = T.nnet.sigmoid(Wox + T.dot(self.W_ho, h_tm1) + self.w_co * c_t + self.b_o)
                            
        h_t = o_t * T.tanh(c_t)

        return h_t, c_t

class LstmNIG(LstmBase):

    def __init__(self, rng, x, n_in, n_h):

        LstmBase.__init__(self, rng, x, n_in, n_h)

        self.params = [self.W_xf, self.W_hf, self.w_cf,
                       self.W_xo, self.W_ho, self.w_co, 
                       self.W_xc, self.W_hc,
                       self.b_f, 
                       self.b_o, self.b_c]
                       
    def lstm_as_activation_function(self, Wix, Wfx, Wcx, Wox, h_tm1, c_tm1):
    
        f_t = T.nnet.sigmoid(Wfx + T.dot(self.W_hf, h_tm1) + self.w_cf * c_tm1 + self.b_f)  # 
    
        c_t = f_t * c_tm1 + T.tanh(Wcx + T.dot(self.W_hc, h_tm1) + self.b_c)  #i_t * 

        o_t = T.nnet.sigmoid(Wox + T.dot(self.W_ho, h_tm1) + self.w_co * c_t + self.b_o)
                            
        h_t = o_t * T.tanh(c_t)

        return h_t, c_t


class LstmNOG(LstmBase):

    def __init__(self, rng, x, n_in, n_h):

        LstmBase.__init__(self, rng, x, n_in, n_h)

        self.params = [self.W_xi, self.W_hi, self.w_ci,
                       self.W_xf, self.W_hf, self.w_cf,
                       self.W_xc, self.W_hc,
                       self.b_i, self.b_f, 
                       self.b_c]
                       
    def lstm_as_activation_function(self, Wix, Wfx, Wcx, Wox, h_tm1, c_tm1):
    
        i_t = T.nnet.sigmoid(Wix + T.dot(self.W_hi, h_tm1) + self.w_ci * c_tm1 + self.b_i)  #
        f_t = T.nnet.sigmoid(Wfx + T.dot(self.W_hf, h_tm1) + self.w_cf * c_tm1 + self.b_f)  # 
    
        c_t = f_t * c_tm1 + i_t * T.tanh(Wcx + T.dot(self.W_hc, h_tm1) + self.b_c)  #i_t * 

        h_t = T.tanh(c_t)

        return h_t, c_t


class LstmNoPeepholes(LstmBase):

    def __init__(self, rng, x, n_in, n_h):

        LstmBase.__init__(self, rng, x, n_in, n_h)

        self.params = [self.W_xi, self.W_hi, #self.W_ci,
                       self.W_xf, self.W_hf, #self.W_cf,
                       self.W_xo, self.W_ho, #self.W_co, 
                       self.W_xc, self.W_hc,
                       self.b_i, self.b_f, 
                       self.b_o, self.b_c]
                       
    def lstm_as_activation_function(self, Wix, Wfx, Wcx, Wox, h_tm1, c_tm1):
    
        i_t = T.nnet.sigmoid(Wix + T.dot(self.W_hi, h_tm1) + self.b_i)
        f_t = T.nnet.sigmoid(Wfx + T.dot(self.W_hf, h_tm1) + self.b_f)
    
        c_t = f_t * c_tm1 + i_t * T.tanh(Wcx + T.dot(self.W_hc, h_tm1) + self.b_c)

        o_t = T.nnet.sigmoid(Wox + T.dot(self.W_ho, h_tm1) + self.b_o)
                            
        h_t = o_t * T.tanh(c_t)

        return h_t, c_t
        

class SimplifiedLstm(LstmBase):

    def __init__(self, rng, x, n_in, n_h):
        
        LstmBase.__init__(self, rng, x, n_in, n_h)

        self.params = [self.W_xf, self.W_hf,
                       self.W_xc, self.W_hc,
                       self.b_f,  self.b_c]
                       
        self.L2_cost = (self.W_xf ** 2).sum() + (self.W_hf ** 2).sum() + (self.W_xc ** 2).sum() + (self.W_hc ** 2).sum()
        
    def lstm_as_activation_function(self, Wix, Wfx, Wcx, Wox, h_tm1, c_tm1):
    
        f_t = T.nnet.sigmoid(Wfx + T.dot(h_tm1, self.W_hf) + self.b_f)  #self.w_cf * c_tm1 
    
        c_t = f_t * c_tm1 + (1 - f_t) * T.tanh(Wcx + T.dot(h_tm1, self.W_hc) + self.b_c) 

        h_t = T.tanh(c_t)

        return h_t, c_t


class BidirectionSLstm(SimplifiedLstm):

    def __init__(self, rng, x, n_in, n_h, n_out):
        
        fwd = SimplifiedLstm(rng, x, n_in, n_h)
        bwd = SimplifiedLstm(rng, x[::-1], n_in, n_h)

        self.params = fwd.params + bwd.params

        self.output = T.concatenate([fwd.output, bwd.output[::-1]], axis=1)

class BidirectionLstm(VanillaLstm):

    def __init__(self, rng, x, n_in, n_h, n_out):
        
        fwd = VanillaLstm(rng, x, n_in, n_h)
        bwd = VanillaLstm(rng, x[::-1], n_in, n_h)

        self.params = fwd.params + bwd.params

        self.output = T.concatenate([fwd.output, bwd.output[::-1]], axis=1)


class RecurrentOutput(object):
    def __init__(self, rng, x, n_in, n_out):
    
        self.W_h = theano.shared(value=np.asarray(rng.normal(0.0, 1.0/np.sqrt(n_out), size=(n_in, n_out)), dtype=config.floatX), name='W_h')
        self.W_y = theano.shared(value=np.asarray(rng.normal(0.0, 1.0/np.sqrt(n_out), size=(n_out, n_out)), dtype=config.floatX), name='W_y')
        
        self.b_y = theano.shared(value=np.zeros((n_out, ), dtype=config.floatX), name='b_y')
        
        


# Gated Recurrent Unit
# 
class GRU(object):
    def __init__(self, n_in, n_h):

        self.n_in = int(n_in)
        self.n_h = int(n_h)

        rng = np.random.RandomState(123)
        
        # Here we define 6 weight matrices and 3 bias vector.
        # To determine the dimention of weight matrices and bias vectors,
        # we just need to have the following numbers: n_in, n_h
        # (You can check it using the equations).
        #
        # Thus:
        # <var>: <dimention>
        #
        # W_xz : n_h  x  n_in
        # W_hz : n_h  x  n_h
        # W_xr : n_h  x  n_in
        # W_hr : n_h  x  n_h
        # W_xh : n_h  x  n_h
        # W_hh : n_h  x  n_h
        #
        # b_z : n_h  x  1
        # b_r : n_h  x  1
        # b_h : n_h  x  1

        # Update gate weights
        self.W_xz = theano.shared(value = np.asarray(rng.normal(0.0, 1.0/np.sqrt(n_in),
                     size=(n_h, n_in)), dtype=config.floatX),
                                  name = 'W_xz')
        self.W_hz = theano.shared(value = np.asarray(rng.normal(0.0, 1.0/np.sqrt(n_h),
                     size=(n_h, n_h)), dtype=config.floatX),
                                  name = 'W_hz')

        # Reset gate weights
        self.W_xr = theano.shared(value = np.asarray(rng.normal(0.0, 1.0/np.sqrt(n_in),
                     size=(n_h, n_in)), dtype=config.floatX),
                                  name = 'W_xr')
        self.W_hr = theano.shared(value = np.asarray(rng.normal(0.0, 1.0/np.sqrt(n_h),
                     size=(n_h, n_h)), dtype=config.floatX),
                                  name = 'W_hr')

        # Other weights :-)
        self.W_xh = theano.shared(value = np.asarray(rng.normal(0.0, 1.0/np.sqrt(n_in),
                     size=(n_h, n_in)), dtype=config.floatX),
                                  name = 'W_xh')
        self.W_hh = theano.shared(value = np.asarray(rng.normal(0.0, 1.0/np.sqrt(n_h),
                     size=(n_h, n_h)), dtype=config.floatX),
                                  name = 'W_hh')

        # Update gate bias
        self.b_z = theano.shared(value = np.zeros(
                                             (n_h, ),
                                             dtype = config.floatX),
                                 name = 'b_z')

        # Reset gate bias
        self.b_r = theano.shared(value = np.zeros(
                                             (n_h, ),
                                             dtype = config.floatX),
                                 name = 'b_r')

        # Hidden layer bias
        self.b_h = theano.shared(value = np.zeros(
                                             (n_h, ),
                                             dtype = config.floatX),
                                 name = 'b_h')

        self.params = [self.W_xz, self.W_hz, self.W_xr, self.W_hr, 
                          self.W_xh, self.W_hh, self.b_z, self.b_r, 
                          self.b_h]


    def gru_as_activation_function(self, wzx, wrx, whx, h_tm1):
        # update gate   #T.dot(self.W_xz, x_t) 
        z_t = T.nnet.sigmoid(wzx + T.dot(self.W_hz, h_tm1) + self.b_z)
        
        # reset gate #T.dot(self.W_xr, x_t)
        r_t = T.nnet.sigmoid(wrx + T.dot(self.W_hr, h_tm1) + self.b_r)
        
        # candidate h_t  #T.dot(self.W_xh, x_t)
        can_h_t = T.tanh(whx + r_t * T.dot(self.W_hh, h_tm1) + self.b_h)
        
        # h_t
        h_t = (1 - z_t) * h_tm1 + z_t * can_h_t

        return h_t


    
