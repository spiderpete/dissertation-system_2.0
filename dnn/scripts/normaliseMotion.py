#! /bin/python

import sys
import os
import string
import struct
#import numpy as N
import array

dataFile1=sys.argv[1]
dataFile, ext= os.path.splitext(dataFile1)

f=open(dataFile1,'r')
data=f.readlines()
f.close

l=len(data[0].split())
sumData=[0.0] * l
for line in data:
    tList=line.split()
    i=0
    #sumData=[0.0] * l
    for var in tList:
        #print float(var)
        sumData[i]=sumData[i]+float((var))
        #print sumData
        i=i+1
    #tmpData.append(sumData)
    
myL=len(data)
meanData=[]
for m in sumData:
    meanData.append(m/myL)

outData=[]
for line in data:
    tList=line.split()
    i=0
    tData=[]
    for var in tList:
        tData.append(str(float(var)-meanData[i]))
        i+=1
    outData.append(' '.join(tData)+'\n')

                                            
#outFile=dataFile.split('.')

f=open(dataFile+'.norm','w')
f.writelines(outData)
f.close
